package jp.co.kenshu;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import jp.co.kenshu.mapping.EmployeeData;

@Component("itemWriter")
public class EmployeeWriter implements ItemWriter<EmployeeData> {

    private static final Log log = LogFactory.getLog(ExampleItemWriter.class);

    // @Override
    public void write(List<? extends EmployeeData> data) throws Exception {
        for (EmployeeData emp : data) {
            log.info(emp.getId() + ":" + emp.getName() + "/" + emp.getNote());
        }
    }

}
